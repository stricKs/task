import { LeaderService } from './../services/leader.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-leaders',
  templateUrl: './leaders.component.html',
  styleUrls: ['./leaders.component.scss']
})
export class LeadersComponent implements OnInit {

  leaders: any[];

  constructor(private leadersService: LeaderService) { }

  async ngOnInit(): Promise<void> {
    this.leaders = await this.leadersService.getLeaders().toPromise();
  }


}
