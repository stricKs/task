import { environment } from './../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";


@Injectable()
export class HttpService {

  private apiUrl: string = environment.apiUrl;


  constructor(private http: HttpClient) {

  }


  public get(path: string): Observable<any> {
    return this.http.get(this.apiUrl + path);
  }


}