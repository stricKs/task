import { UserService } from './../../services/user.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from "rxjs";
import { User } from "../model/user";

@Component({
  selector: 'list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit, OnDestroy {

  users: User[];
  usersSubscription: Subscription;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.usersSubscription = this.userService.users.subscribe(users => {
      this.users = users;
    });
  }

  ngOnDestroy(): void {
    this.usersSubscription.unsubscribe();
  }

  removeUser(index: number): void {
    this.userService.removeUser(index);
  }

}
