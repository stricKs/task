import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpService } from "./http.service";

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private httpService: HttpService) { }


  getLeaders(): Observable<any> {
    return this.httpService.get("/leaders");
  }
}
