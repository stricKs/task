


export class ContactInfo {

  email: string;
  phone: string;


  get getInfo(): string {
    if (this.email && this.phone) return this.email + " | " + this.phone;
    else if (this.email) return this.email;
    else if (this.phone) return this.phone;
    else return "no contact information";
  }

}