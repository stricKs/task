import { UserService } from './../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { User } from "../model/user";
import { Gender } from "../enum/gender";

@Component({
  selector: 'add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  userForm: FormGroup;


  constructor(private fb: FormBuilder,
              private userService: UserService) { 

  }


  ngOnInit(): void {
    this.buildForm();

  }

  buildForm(): void {
    this.userForm = this.fb.group({
      name: ["", Validators.required],
      gender: [Gender.MALE],
      contactInfo: this.fb.group({
        email: ["", Validators.email],
        phone: [""]
      })
    });
  }

  addUser(): void {
    this.userForm.markAllAsTouched();
    if (!this.userForm.valid) return;
    const user: User = User.buildFromForm(this.userForm.value);
    this.userService.addUser(user);
    this.resetForm();
  }

  resetForm(): void {
    this.buildForm();
  }

  showControlError(controlPath: string): boolean {
    let control: FormControl = this.getFormControlFromPath(controlPath);
    if (!control) return false;
    return (control.dirty || control.touched) && !control.valid;
  }

  getFormControlFromPath(controlPath: string): FormControl {
    let path: string[] = controlPath.split("|");
    let formGroup: FormGroup = this.userForm;
    let i;
    for (i = 0; i < path.length - 1; i++) {
      formGroup = formGroup.get(path[i]) as FormGroup;
    }
    return formGroup.get(path[i]) as FormControl;
  }

}
