import { Gender } from "../enum/gender";
import { ContactInfo } from "./contact-info";



export class User {

  name: string;
  gender: Gender;

  contactInfo: ContactInfo = new ContactInfo();

  constructor() {
    
  }


  public static buildFromForm(data: any): User {
    let u = new User();
    // maybe use cloneDeep
    u.name = data.name;
    u.gender = data.gender;
    u.contactInfo.email = data.contactInfo.email;
    u.contactInfo.phone = data.contactInfo.phone;
    return u;
  }

  

}