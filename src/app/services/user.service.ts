import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { User } from "../users/model/user";
import { HttpService } from "./http.service";

@Injectable()
export class UserService {

  users: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  

  constructor(private httpService: HttpService) { }


  addUser(user: User): void {
    let currentValues = this.users.value;
    currentValues.push(user);
    this.users.next(currentValues);
  }

  removeUser(index: number): void {
    let currentValues = this.users.value;
    currentValues.splice(index, 1);
    this.users.next(currentValues);
  }

  getUsers(): void {
    this.httpService.get("/leaders").subscribe(leaders => {
      
    });
  }


  

}
